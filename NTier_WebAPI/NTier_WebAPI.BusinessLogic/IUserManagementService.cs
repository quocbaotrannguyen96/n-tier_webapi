﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTier_WebAPI.Modelss;

namespace NTier_WebAPI.BusinessLogic
{
    public interface IUserManagementService
    {
        user GetUserById(int id);
        IEnumerable<user> GetAllUsers();
        int CreateUser(user userEntity);
        bool UpdateUser(int id, user userEntity);
        bool DeleteUser(int id);
    }
}
