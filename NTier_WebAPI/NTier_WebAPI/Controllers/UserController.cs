﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NTier_WebAPI.BusinessLogic;
using NTier_WebAPI.Modelss;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace NTier_WebAPI.Controllers
{
    public class UserController : ApiController
    {
        private readonly IUserManagementService _userServices;                
        public UserController(IUserManagementService userManagementService)
        {
            _userServices = userManagementService;
        }

        // GET api/user
        public HttpResponseMessage Get()
        {
            var users = _userServices.GetAllUsers();
            if (users != null)
            {
                var userEntities = users as List<user> ?? users.ToList();
                if (userEntities.Any())
                    return Request.CreateResponse(HttpStatusCode.OK, userEntities);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Users not found");
        }

        // GET api/user/5
        public HttpResponseMessage Get(int id)
        {
            var user = _userServices.GetUserById(id);
            if (user != null)
                return Request.CreateResponse(HttpStatusCode.OK, user);
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No user found for this id");
        }

        // POST api/user
        public int Post([FromBody] user userEntity)
        {
            return _userServices.CreateUser(userEntity);
        }

        // PUT api/user/5
        public bool Put(int id, [FromBody]user userEntity)
        {
            if (id > 0)
            {
                return _userServices.UpdateUser(id, userEntity);
            }
            return false;
        }

        // DELETE api/user/5
        public bool Delete(int id)
        {
            if (id > 0)
                return _userServices.DeleteUser(id);
            return false;
        }
    }
}
